package main;

import controller.Implementations.LoginControllerImpl;
import main.Implementations.CellsManagerImpl;
import main.Implementations.GuardsManagerImpl;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {
	
	/**
     * Program main, this is the "root" of the application.
     * @param args
     * unused,ignore
     */
	public static void main(final String... args){
		new LoginControllerImpl(new LoginView());
	}
}
