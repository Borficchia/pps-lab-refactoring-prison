package main.Interfaces;

import model.Interfaces.Guard;

import java.util.List;

public interface GuardsManager {
    List<Guard> getListGuards();
    boolean removeGuard(int id);
    Guard getGuard(int id);
    boolean insertGuard(Guard guard);
}
