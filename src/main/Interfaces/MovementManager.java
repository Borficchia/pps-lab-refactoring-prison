package main.Interfaces;

import model.Interfaces.Movement;

import java.util.List;

public interface MovementManager {

    List<Movement> getMovementList();
    void addMovement(Movement movement);
}
