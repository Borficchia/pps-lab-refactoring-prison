package main.Interfaces;

import model.Interfaces.Cell;

import java.util.List;

public interface CellsManager {

    Cell getCell(int id);
    List<Cell> getCellsList();
    boolean isFull(int id);
    void incrementPrisonerInCell(int cellID);
    void decrementPrisonerInCell(int cellID);
}
