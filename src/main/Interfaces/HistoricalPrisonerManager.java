package main.Interfaces;

import model.Interfaces.Prisoner;

import java.util.List;

public interface HistoricalPrisonerManager {
    List<Prisoner> getHistoryPrisoners();
    void insertPrisoner(Prisoner prisoner);
    Prisoner getPrisonerByID(int id);
}
