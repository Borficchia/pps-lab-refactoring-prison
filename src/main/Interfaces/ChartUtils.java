package main.Interfaces;

public interface ChartUtils {

    void createBarChart();

    void createPieChart();

}
