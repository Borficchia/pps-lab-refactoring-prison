package main.Interfaces;

import model.Interfaces.Prisoner;

import java.util.List;

public interface PrisonerManager extends ObjectManager<Prisoner>{
    List<Prisoner> getCurrentPrisoners();
    Prisoner getCurrentPrisoner(int id);
    void insertPrisoner(Prisoner prisoner);
    boolean removePrisoner(int id);
}
