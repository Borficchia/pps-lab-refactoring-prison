package main.Interfaces;

import model.Interfaces.Visitor;

import java.util.List;

public interface VisitorsManager {
    List<Visitor> getVisitorList();
    void addVisitor(Visitor visitor);
}
