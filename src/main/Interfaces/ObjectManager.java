package main.Interfaces;

import java.util.List;

public interface ObjectManager<E> {
    List<E> getList();
    void addElement(E element);
}
