package main.factories;

import main.Implementations.*;
import main.Interfaces.*;

public class ManagerFactory {

    public static CellsManager getCellsManager() {
        return CellsManagerImpl.getInstance();
    }

    public static GuardsManager getGuardsManager() {
        return GuardsManagerImpl.getInstance();
    }

    public static MovementManager getMovementManager(){
        return  MovementManagerImpl.getInstance();
    }

    public static PrisonerManager getPrisonerManager(){
        return PrisonerManagerImpl.getInstance();
    }

    public static HistoricalPrisonerManager getHistoricalPrisonerManagerImpl(){
        return HistoricalPrisonerManagerImpl.getInstance();
    }

    public static VisitorsManager getVisitorsManger(){
        return  VisitorsManagerImpl.getInstance();
    }
}
