package main.Implementations;

import main.Interfaces.HistoricalPrisonerManager;
import main.Interfaces.ObjectManager;
import main.Interfaces.PrisonerManager;
import main.factories.ManagerFactory;
import model.Interfaces.Prisoner;

import java.util.List;

public class PrisonerManagerImpl extends ObjectManagerImpl<Prisoner> implements PrisonerManager {

    private static PrisonerManager SINGLETON = null;

    private static String getCurrentPrisonerPath(){ return getResourcesPath() + "/CurrentPrisoners.txt"; }

    private PrisonerManagerImpl() {
        super(getCurrentPrisonerPath());
    }

    public static PrisonerManager getInstance(){
        if (SINGLETON == null){
            SINGLETON = new PrisonerManagerImpl();
        }
        return SINGLETON;
    }

    @Override
    public List<Prisoner> getCurrentPrisoners() {
        return getList();
    }


    @Override
    public Prisoner getCurrentPrisoner(int id){
        for(Prisoner prisonder : getCurrentPrisoners()){
            if(prisonder.getPrisonderId() == id){
                return prisonder;
            }
        }
        return null;
    }

    @Override
    public void insertPrisoner(Prisoner prisoner){
        if (getCurrentPrisoner(prisoner.getPrisonderId()) == null){
            super.addElement(prisoner);
        }
    }

    @Override
    public boolean removePrisoner(int id){
        if (getCurrentPrisoner(id) != null){
            getCurrentPrisoners().remove(getCurrentPrisoner(id));
            saveListInFile(getCurrentPrisoners());
            return true;
        }
        return false;
    }
}
