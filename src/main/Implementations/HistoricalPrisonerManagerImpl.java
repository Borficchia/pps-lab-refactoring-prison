package main.Implementations;

import main.Interfaces.HistoricalPrisonerManager;
import model.Interfaces.Prisoner;

import java.util.List;

public class HistoricalPrisonerManagerImpl extends ObjectManagerImpl<Prisoner> implements HistoricalPrisonerManager {


        private static HistoricalPrisonerManager SINGLETON = null;

        private static String getHistoryPrisonerPath(){
            return getResourcesPath() + "/Prisoners.txt";
        }


        private HistoricalPrisonerManagerImpl() {
            super(getHistoryPrisonerPath());
        }

        public static HistoricalPrisonerManager getInstance(){
            if (SINGLETON == null){
                SINGLETON = new HistoricalPrisonerManagerImpl();
            }
            return SINGLETON;
        }

        @Override
        public List<Prisoner> getHistoryPrisoners() {
            return super.getList();
        }

        @Override
        public Prisoner getPrisonerByID(int id){
            for(Prisoner prisonder : getHistoryPrisoners()){
                if(prisonder.getPrisonderId() == id){
                    return prisonder;
                }
            }
            return null;
        }

        @Override
        public void insertPrisoner(Prisoner prisoner){
            if (getPrisonerByID(prisoner.getPrisonderId()) == null){
                super.addElement(prisoner);
            }
        }
}
