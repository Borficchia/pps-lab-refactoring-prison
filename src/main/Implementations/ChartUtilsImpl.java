package main.Implementations;

import main.Interfaces.ChartUtils;
import main.Interfaces.HistoricalPrisonerManager;
import main.Interfaces.PrisonerManager;
import main.factories.ManagerFactory;
import model.Interfaces.Prisoner;
import view.Components.BarChart_AWT;
import view.Components.PieChart_AWT;

import javax.swing.*;
import java.util.*;

public class ChartUtilsImpl implements ChartUtils {

    private PrisonerManager prisonerManager;
    private HistoricalPrisonerManager historicalPrisonerManager;

    public ChartUtilsImpl() {
        this.historicalPrisonerManager = ManagerFactory.getHistoricalPrisonerManagerImpl();
        this.prisonerManager = ManagerFactory.getPrisonerManager();
    }

    @Override
    public void createBarChart(){

        //creo una mappa contenente gli anni e il numero dei prigioneri in quell anno
        Map<Integer,Integer> map=new TreeMap<>();
        //anno in cui ha aperto la prigione
        final int OPENING=2017;
        //salvo la lista di prigionieri
        List<Prisoner> list = historicalPrisonerManager.getHistoryPrisoners();
        //recupero l'anno massimo in cui un prigioniero è imprigionato
        int max=getMax();
        //ciclo tutti gli anni e modifico il numero di prigionieri
        for(int i=OPENING;i<=max;i++){
            int num = 0;;
            for(Prisoner prisoner:list){
                Calendar calendar = Calendar.getInstance();
                Calendar calendar2 = Calendar.getInstance();
                calendar.setTime(prisoner.getInizio());
                calendar2.setTime(prisoner.getFine());
                if(calendar.get(Calendar.YEAR) <= i && calendar2.get(Calendar.YEAR) >= i){
                    num++;
                }
            }
            map.put(i, num);
        }
        //creo il grafico
        BarChart_AWT chart = new BarChart_AWT(map,"Numero prigionieri per anno","Numero prigionieri per anno");
        chart.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }

    @Override
    public void createPieChart(){

        //salvo le categorie di reati in un vettore che poi converto in lista
        String[] crimes = {"Reati contro gli animali","Reati associativi","Blasfemia e sacrilegio","Reati economici e finanziari","Falsa testimonianza","Reati militari","Reati contro il patrimonio","Reati contro la persona","Reati nell' ordinamento italiano","Reati tributari","Traffico di droga","Casi di truffe"};
        ArrayList<String> crimesList = new ArrayList<>(Arrays.asList(crimes));
        List<Prisoner> list = prisonerManager.getCurrentPrisoners();

        //creo una mappa in cui inserire come chiave il reato e come valore il numero di prigionieri che l'anno commesso
        Map<String,Integer>map=new HashMap<>();
        for(String s : crimesList){
            map.put(s, 0);
        }
        for(Prisoner p: list){
            for(String s : p.getCrimini()){
                if(crimesList.contains(s)){
                    map.put(s, map.get(s)+1);
                }
            }
        }
        //creo il grafico
        PieChart_AWT pie = new PieChart_AWT("Percentuale crimini commessi dai reclusi attuali",map);
        pie.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private int getMax(){
        int max=0;
        for(Prisoner p: prisonerManager.getCurrentPrisoners()){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(p.getFine());
            if(calendar.get(Calendar.YEAR)>max){
                max=calendar.get(Calendar.YEAR);
            }
        }
        return max;
    }
}
