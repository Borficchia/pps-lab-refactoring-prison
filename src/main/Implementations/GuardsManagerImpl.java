package main.Implementations;

import main.Interfaces.GuardsManager;
import model.Interfaces.Guard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GuardsManagerImpl extends ObjectManagerImpl<Guard> implements GuardsManager {

    private static GuardsManager SINGLETON = null;

    private String getDatePattern() {
        return "MM/dd/yyyy";
    }

    private static String getGuardieUserPath(){
        return getResourcesPath() + "/GuardieUserPass.txt";
    }

    public static GuardsManager getInstance(){
        if (SINGLETON == null){
            SINGLETON = new GuardsManagerImpl();
        }
        return SINGLETON;
    }

    private GuardsManagerImpl() {
        super(getGuardieUserPath());
    }

    @Override
    public List<Guard> getListGuards(){
        return getList();
    }

    @Override
    public boolean removeGuard(int id){
        if(getGuard(id) != null){
            getListGuards().remove(getGuard(id));
            saveListInFile(getListGuards());
            return true;
        }
        return false;
    }

    @Override
    public Guard getGuard(int id){
        for(Guard guard : getListGuards()){
            if(guard.getID() == id){
               return guard;
            }
        }
        return null;
    }

    @Override
    public boolean insertGuard(Guard guard){
        if( getGuard(guard.getID()) == null ){
            super.addElement(guard);
            return true;
        }
        return false;
    }
}