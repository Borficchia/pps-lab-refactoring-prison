package main.Implementations;

import main.Interfaces.CellsManager;
import model.Interfaces.Cell;

import java.util.List;

public class CellsManagerImpl extends ObjectManagerImpl<Cell> implements CellsManager {

    private static CellsManager SINGLETON = null;

    private static String getCellsPath(){
        return getResourcesPath() + "/Celle.txt";
    }

    public static CellsManager getInstance(){
        if (SINGLETON == null){
            SINGLETON = new CellsManagerImpl();
        }
        return SINGLETON;
    }

    private CellsManagerImpl( ) {
        super(getCellsPath());
    }

    @Override
    public Cell getCell(int id){
        for(Cell cell : getCellsList()){
            if(cell.getId() == id){
                return cell;
            }
        }
        return null;
    }

    @Override
    public List<Cell> getCellsList() {
        return super.getList();
    }

    @Override
    public boolean isFull(int id){
        Cell cell = getCell(id);
        if (cell.getCapacity() - cell.getCurrentPrisoners() == 0){
            return true;
        }
        return  false;
    }

    @Override
    public void incrementPrisonerInCell(int cellID) {
        getCell(cellID).setCurrentPrisoners(getCell(cellID).getCurrentPrisoners() + 1);
        saveListInFile(getCellsList());
    }

    @Override
    public void decrementPrisonerInCell(int cellID) {
        getCell(cellID).setCurrentPrisoners(getCell(cellID).getCurrentPrisoners() - 1);
        saveListInFile(getCellsList());
    }
}
