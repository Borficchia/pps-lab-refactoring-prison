package main.Implementations;

import main.Interfaces.ObjectManager;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class ObjectManagerImpl<E> implements ObjectManager<E> {

    private List<E> objList;
    private File file;

    protected  static String getResourcesPath(){
        return "res";
    }

    public ObjectManagerImpl(String path){
        new File(getResourcesPath()).mkdir();
        file = new File(path);
        this.objList = getListFromFile();
    }


    protected void saveListInFile(List<E> object) {
        File file = this.file;
        if (file.length() >= 0){
            FileOutputStream fo;
            ObjectOutputStream os = null;
            try {
                fo = new FileOutputStream(file);
                os = new ObjectOutputStream(fo);
                os.flush();
                fo.flush();
                for(E obgectToWrite : object){
                    os.writeObject(obgectToWrite);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected List<E> getListFromFile(){
        File file = this.file;
        if (file.length() > 0){
            List<E> objectList = new ArrayList<E>();
            //se il file è vuoto restituisco un file vuoto
            if(file.length()!= 0){

                FileInputStream fileInputStream = null;
                ObjectInputStream objectInputStream = null;
                try {
                    fileInputStream = new FileInputStream(file);
                    objectInputStream = new ObjectInputStream(fileInputStream);

                    objectList = new ArrayList<>();

                    while (true) {
                        //salvo ogni oggetto in una lista
                        E s = (E)objectInputStream.readObject();
                        objectList.add(s);
                    }

                } catch (EOFException e) {
                    return objectList;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        fileInputStream.close();
                        objectInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return new ArrayList<E>();
    }

    @Override
    public List<E> getList(){
        return this.objList;
    }

    @Override
    public void addElement(E element){
        objList.add(element);
        saveListInFile(getList());
    }
}