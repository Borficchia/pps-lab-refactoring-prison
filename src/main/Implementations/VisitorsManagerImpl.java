package main.Implementations;

import main.Interfaces.VisitorsManager;
import model.Interfaces.Visitor;

import javax.swing.*;
import java.util.List;

public class VisitorsManagerImpl extends ObjectManagerImpl<Visitor> implements VisitorsManager{

    private static VisitorsManager SINGLETON = null;

    private static String getVisitorsPath(){ return getResourcesPath() + "/Visitors.txt";}

    private VisitorsManagerImpl(){
        super(getVisitorsPath());
    }

    public static VisitorsManager getInstance(){
        if (SINGLETON == null) {
            SINGLETON = new VisitorsManagerImpl();
        }
        return SINGLETON;
    }

    @Override
    public List<Visitor> getVisitorList() {
        return super.getList();
    }

    @Override
    public void addVisitor(Visitor visitor) {
        super.addElement(visitor);
    }
}
