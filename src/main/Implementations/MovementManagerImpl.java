package main.Implementations;

import main.Interfaces.MovementManager;
import model.Interfaces.Movement;

import java.util.List;

public class MovementManagerImpl extends ObjectManagerImpl<Movement> implements MovementManager {

    private static MovementManager SINGLETON = null;

    public static MovementManager getInstance(){
        if ( SINGLETON == null){
            SINGLETON = new MovementManagerImpl();
        }
        return SINGLETON;
    }

    private static String getMovementPath(){
        return getResourcesPath() + "/AllMovements.txt";
    }

    private MovementManagerImpl() {
        super(getMovementPath());
    }

    @Override
    public List<Movement> getMovementList() {
        return getList();
    }

    @Override
    public void addMovement(Movement movement){
        super.addElement(movement);
    }
}
