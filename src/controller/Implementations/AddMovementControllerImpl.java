package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.Implementations.MovementManagerImpl;
import main.Interfaces.MovementManager;
import main.factories.ManagerFactory;
import model.Implementations.MovementImpl;
import model.Interfaces.Movement;
import view.Interfaces.AddMovementView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller che gestisce la addMovementView
 */
public class AddMovementControllerImpl {

	static AddMovementView addMovementView;
	private MovementManager movementManager;
	
	/**
	 * costruttore 
	 * @param addMovementView la view
	 */
	public AddMovementControllerImpl(AddMovementView addMovementView){
		AddMovementControllerImpl.addMovementView=addMovementView;
		AddMovementControllerImpl.addMovementView.addBackListener(new BackListener());
		AddMovementControllerImpl.addMovementView.addInsertListener(new InsertListener());

		movementManager = ManagerFactory.getMovementManager();
	}
	
	/** torna alla view precedente */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			addMovementView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(addMovementView.getRank()));
		}
	}
	
	/**
	 * listener che inserisce un movimento
	 */
	public class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//prendo il movimento dalla view
			Movement movement = new MovementImpl(addMovementView.getDesc(),addMovementView.getValue(),addMovementView.getSymbol().charAt(0));
			//se l'amount è maggiore di zero inserisco il movimento
			if(movement.getAmount() > 0){
				movementManager.addMovement(movement);
				addMovementView.displayErrorMessage("Movimento inserito");
			}else {
				addMovementView.displayErrorMessage("Input invalido");
			}
		}
	}
}
	
	
	

