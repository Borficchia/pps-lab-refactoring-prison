package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import main.Interfaces.PrisonerManager;
import main.Interfaces.VisitorsManager;
import main.factories.ManagerFactory;
import model.Interfaces.Visitor;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller della addVisitorsView
 */
public class AddVisitorsControllerImpl {
	
	static AddVisitorsView visitorsView;
	private PrisonerManager prisonerManagerImpl;
	private VisitorsManager visitorsManager;
	/**
	 * costruttore
	 * @param view la view
	 */
	public AddVisitorsControllerImpl(AddVisitorsView view)
	{
		AddVisitorsControllerImpl.visitorsView=view;
		visitorsView.addBackListener(new BackListener());
		visitorsView.addInsertVisitorListener(new InsertListener());
		prisonerManagerImpl = ManagerFactory.getPrisonerManager();
		visitorsManager = ManagerFactory.getVisitorsManger();
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(visitorsView.getRank()));
		}
	}
	
	/**
	 * listener che gestisce l'inserimento di visitatori
	 */
	public class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//salvo il visitatore inserito nella view
			Visitor visitor = visitorsView.getVisitor();
			//controllo che non ci siano errori
			if (visitor.getName().length() > 2 && visitor.getSurname().length() > 2 && prisonerManagerImpl.getCurrentPrisoner(visitor.getPrisonerID()) != null) {
				//si può inserire il visitatore
				visitorsManager.addVisitor(visitor);
				visitorsView.displayErrorMessage("Visitatore inserito");
			} else {
				visitorsView.displayErrorMessage("Devi inserire un nome, un cognome e un prigioniero esistente");
			}
		}
	}
}



