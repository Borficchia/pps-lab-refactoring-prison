package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.Implementations.ChartUtilsImpl;
import main.Implementations.PrisonerManagerImpl;
import main.Interfaces.ChartUtils;
import main.Interfaces.PrisonerManager;
import main.factories.ManagerFactory;
import view.Interfaces.AddMovementView;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.BalanceView;
import view.Interfaces.MainView;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.ViewCellsView;
import view.Interfaces.ViewVisitorsView;

/**
 * controller della more functions view
 */
public class MoreFunctionsControllerImpl {

	 MoreFunctionsView moreFunctionsView;
	// private PrisonerManager prisonerManager;
	 private ChartUtils chartUtils;
	
	 /**
	  * costruttore
	  * @param moreFunctionsView la view
	  */
	public MoreFunctionsControllerImpl(MoreFunctionsView moreFunctionsView){
		this.moreFunctionsView=moreFunctionsView;
		moreFunctionsView.addBackListener(new BackListener());
		moreFunctionsView.addAddMovementListener(new AddMovementListener());
		moreFunctionsView.addBalanceListener(new BalanceListener());
		moreFunctionsView.addChart1Listener(new Chart1Listener());
		moreFunctionsView.addChart2Listener(new Chart2Listener());
		moreFunctionsView.addAddVisitorsListener(new AddVisitorsListener());
		moreFunctionsView.addViewVisitorsListener(new ViewVisitorsListener());
		moreFunctionsView.addViewCellsListener(new ViewCellsListener());

		//prisonerManager = ManagerFactory.getPrisonerManager();
		chartUtils = new ChartUtilsImpl();
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 * @author Utente
	 *
	 */
	public class BackListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			moreFunctionsView.dispose();
			new MainControllerImpl(new MainView(moreFunctionsView.getRank()));
		}
	}
	
	/**
	 * listener che apre la add movement view
	 */
	public class AddMovementListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			moreFunctionsView.dispose();
			new AddMovementControllerImpl(new AddMovementView(moreFunctionsView.getRank()));
		}
	}
	
	/**
	 * listener che apre la balance view
	 */
	public class BalanceListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new BalanceControllerImpl(new BalanceView(moreFunctionsView.getRank()));
		}
	}
	
	/**
	 * listener che si occupa di aprire il primo grafico 
	 */
	public class Chart1Listener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			chartUtils.createBarChart();
		}
	}
	
	/**
	 * listener che si occupa di lanciare il secondo grafico
	 */
	public class Chart2Listener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			chartUtils.createPieChart();
		}
	}
	
	/**
	 * listener che apre l'add visitors view
	 */
	public class AddVisitorsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new AddVisitorsControllerImpl(new AddVisitorsView(moreFunctionsView.getRank()));
		}
	}
	
	/**
	 * listener che apre la view visitors view
	 */
	public class ViewVisitorsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new ViewVisitorsControllerImpl(new ViewVisitorsView(moreFunctionsView.getRank()));
		}
	}
	
	/**
	 * listener che apre la view cells view
	 */
	public class ViewCellsListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			moreFunctionsView.dispose();
			new ViewCellsControllerImpl(new ViewCellsView(moreFunctionsView.getRank()));
		}
	}
}
