package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.Implementations.GuardsManagerImpl;
import main.factories.ManagerFactory;
import model.Interfaces.Guard;
import view.Interfaces.SupervisorFunctionsView;
import view.Interfaces.ViewGuardView;

/**
 * controller che gestisce la view guard view
 */
public class ViewGuardControllerImpl {

	static ViewGuardView viewGuardView;

	/**
	 * costruttore
	 * @param viewGuardView la view
	 */
	public ViewGuardControllerImpl(ViewGuardView viewGuardView){
		ViewGuardControllerImpl.viewGuardView=viewGuardView;
		viewGuardView.addBackListener(new BackListener());
		viewGuardView.addViewListener(new ViewGuardListener());
	}

	/**
	 * imposta la view in modo da mostrare la guardia
	 */
	public static class ViewGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Guard guard = ManagerFactory.getGuardsManager().getGuard(viewGuardView.getID());
			if (guard != null){
				viewGuardView.setName(guard.getName());
				viewGuardView.setSurname(guard.getSurname());
				viewGuardView.setBirth(guard.getBirthDate().toString());
				viewGuardView.setRank(String.valueOf(guard.getRank()));
				viewGuardView.setTelephone(guard.getTelephoneNumber());
			}else {
				viewGuardView.displayErrorMessage("Guardia non trovata");
			}
		}
	}

	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			viewGuardView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(viewGuardView.getRank()));
		}
	}
}