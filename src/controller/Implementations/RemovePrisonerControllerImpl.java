package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.Interfaces.RemovePrisonerController;
import main.Implementations.CellsManagerImpl;
import main.Implementations.PrisonerManagerImpl;
import main.Interfaces.CellsManager;
import main.Interfaces.PrisonerManager;
import main.factories.ManagerFactory;
import model.Interfaces.Prisoner;
import view.Interfaces.MainView;
import view.Interfaces.RemovePrisonerView;

/**
 * controller della remove prisoner view
 */
public class RemovePrisonerControllerImpl implements RemovePrisonerController{

	static RemovePrisonerView removePrisonerView;
	private PrisonerManager prisonerManager;
	private CellsManager cellsManager;
	
	/**
	 * costruttore
	 * @param removePrisonerView la view
	 */
	public RemovePrisonerControllerImpl(RemovePrisonerView removePrisonerView){
		RemovePrisonerControllerImpl.removePrisonerView=removePrisonerView;
		removePrisonerView.addRemoveListener(new RemoveListener());
		removePrisonerView.addBackListener(new BackListener());

		prisonerManager = ManagerFactory.getPrisonerManager();
		cellsManager = ManagerFactory.getCellsManager();
	}
	
	/**
	 * listener che si occupa di rimuovare il prigioniero
	 */
	public class RemoveListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			removePrisoner();
		}

	}
	
	public void removePrisoner(){
		Prisoner prisoner = prisonerManager.getCurrentPrisoner(removePrisonerView.getID());
		if (prisonerManager.removePrisoner(prisoner.getCellID())){
			cellsManager.decrementPrisonerInCell(prisoner.getCellID());
			removePrisonerView.displayErrorMessage("Prigioniero rimosso");
		}else {
			removePrisonerView.displayErrorMessage("Prigioniero non trovato");
		}
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			removePrisonerView.dispose();
			new MainControllerImpl(new MainView(removePrisonerView.getRank()));
		}
	}
}