package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import main.Implementations.GuardsManagerImpl;
import main.Interfaces.GuardsManager;
import main.factories.ManagerFactory;
import model.Interfaces.Guard;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;

/**
 * controller della login view
 */
public class LoginControllerImpl {

	LoginView loginView;

	/**
	 * costruttore
	 * @param loginView la view
	 */
	public LoginControllerImpl(LoginView loginView){
		this.loginView=loginView;
		loginView.addLoginListener(new LoginListener());
		loginView.displayErrorMessage("accedere con i profili: \n id:3 , password:qwerty \n id:2 , password:asdasd ");
	}

	/**
	 * listener che si occupa di effettuare il login
	 */
	public class LoginListener implements ActionListener{

		List<Guard> guards = null;
		GuardsManager guardsManager = ManagerFactory.getGuardsManager();

		@Override
		public void actionPerformed(ActionEvent arg0) {

			checkGuardList();

			if (!checkEmptyLogin()){
				checkCorrectLogin();
			}
		}

		private void checkGuardList(){
			//salvo la lista delle guardie nel file
			this.guards = this.guardsManager.getListGuards();
		}

		private boolean checkEmptyLogin(){
			if(loginView.getUsername().isEmpty() || loginView.getPassword().isEmpty()){
				loginView.displayErrorMessage("Devi inserire username e password");
				return true;
			}else{
				return false;
			}
		}

		private void checkCorrectLogin(){
			boolean isInside=false;
			for (Guard guard : guards){
				if (loginView.getUsername().equals(String.valueOf(guard.getUsername())) && loginView.getPassword().equals(guard.getPassword())){
					isInside=true;
					loginView.displayErrorMessage("Benvenuto Utente "+ loginView.getUsername());
					loginView.dispose();
					new MainControllerImpl(new MainView(guard.getRank()));
				}
			}
			if (!isInside){
				loginView.displayErrorMessage("Combinazione username/password non corretta");
			}
		}
	}
}
