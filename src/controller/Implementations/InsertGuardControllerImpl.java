package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.Implementations.GuardsManagerImpl;
import main.Interfaces.GuardsManager;
import main.factories.ManagerFactory;
import model.Interfaces.Guard;
import view.Interfaces.InsertGuardView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * controller che gestisce la insert guard view
 */
public class InsertGuardControllerImpl {

	static InsertGuardView insertGuardView;
	private GuardsManager guardsManager = ManagerFactory.getGuardsManager();

	/**
	 * costruttore
	 * @param insertGuardView la view
	 */
	public InsertGuardControllerImpl(InsertGuardView insertGuardView){
		InsertGuardControllerImpl.insertGuardView=insertGuardView;
		insertGuardView.addBackListener(new BackListener());
		insertGuardView.addInsertListener(new InsertListener());
	}

	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuardView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(insertGuardView.getRank()));
		}
	}

	/**
	 * listener che si occupa di inserire una guardia
	 */
	public class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (!isSomethingEmpty(insertGuardView.getGuard())){
				if (guardsManager.insertGuard(insertGuardView.getGuard())){
					insertGuardView.displayErrorMessage("Guardia inserita");
				}else {
					insertGuardView.displayErrorMessage("ID già  usato");
				}
			}else {
				insertGuardView.displayErrorMessage("Completa tutti i campi correttamente");
			}
		}
	}

	public boolean isSomethingEmpty(Guard g){
		if(g.getName().equals("")||g.getSurname().equals("")||g.getRank()<1||g.getRank()>3||g.getID()<0||g.getPassword().length()<6)
			return true;
		return false;
	}
}