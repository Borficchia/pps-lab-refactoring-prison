package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.Implementations.GuardsManagerImpl;
import main.factories.ManagerFactory;
import view.Interfaces.RemoveGuardView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * contoller della remove guard view
 */
public class RemoveGuardControllerImpl {

	static RemoveGuardView removeGuardView;

	/**
	 * costruttore
	 * @param removeGuardView la view
	 */
	public RemoveGuardControllerImpl(RemoveGuardView removeGuardView){
		RemoveGuardControllerImpl.removeGuardView=removeGuardView;
		removeGuardView.addBackListener(new BackListener());
		removeGuardView.addRemoveGuardListener(new RemoveGuardListener());
	}

	/**
	 * listener che si occupa della rimozione della guardia
	 */
	public class RemoveGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if ( ManagerFactory.getGuardsManager().removeGuard(removeGuardView.getID()) == true){
				removeGuardView.displayErrorMessage("Guardia rimossa");
			}else{
				removeGuardView.displayErrorMessage("Guardia non trovata");
			}
		}
	}

	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			removeGuardView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(removeGuardView.getRank()));
		}
	}
}