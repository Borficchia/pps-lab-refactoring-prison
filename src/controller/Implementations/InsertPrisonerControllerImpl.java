package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import main.Implementations.CellsManagerImpl;
import main.Implementations.PrisonerManagerImpl;
import main.Interfaces.CellsManager;
import main.Interfaces.HistoricalPrisonerManager;
import main.Interfaces.PrisonerManager;
import main.factories.ManagerFactory;
import model.Implementations.PrisonerImpl;
import model.Interfaces.Prisoner;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.MainView;

/**
 * controller della view insertprisoner
 */
public class InsertPrisonerControllerImpl {

	static InsertPrisonerView insertPrisonerView;
	private PrisonerManager prisonerManagerImpl;
	private HistoricalPrisonerManager historicalPrisonerManager;
	private CellsManager cellsManagerImpl;
	
	/**
	 * costruttore
	 * @param insertPrisonerView la view
	 */
	public InsertPrisonerControllerImpl(InsertPrisonerView insertPrisonerView) {
		InsertPrisonerControllerImpl.insertPrisonerView=insertPrisonerView;
		insertPrisonerView.addInsertPrisonerListener(new InsertPrisonerListener());
		insertPrisonerView.addBackListener(new BackListener());
		insertPrisonerView.addAddCrimeListener(new AddCrimeListener());
		prisonerManagerImpl = ManagerFactory.getPrisonerManager();
		historicalPrisonerManager = ManagerFactory.getHistoricalPrisonerManagerImpl();
		cellsManagerImpl = ManagerFactory.getCellsManager();
	}
	
	/**
	 * listener che si occupa di inserire prigionieri
	 */
	public class InsertPrisonerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Prisoner prisoner = new PrisonerImpl(insertPrisonerView.getName(), insertPrisonerView.getSurname(), insertPrisonerView.getBirthday(),
					insertPrisonerView.getPrisonerIDtextField(), insertPrisonerView.getStart(), insertPrisonerView.getEndTextfield(),insertPrisonerView.getList(),insertPrisonerView.getCellID());
			if(isSomethingEmpty(prisoner)){
				insertPrisonerView.displayErrorMessage("Completa tutti i campi");
			}else{
				if (checkDate(prisoner)){
					if (!cellsManagerImpl.isFull(prisoner.getCellID())){
						prisonerManagerImpl.insertPrisoner(prisoner);
						historicalPrisonerManager.insertPrisoner(prisoner);
						cellsManagerImpl.incrementPrisonerInCell(prisoner.getCellID());
						insertPrisonerView.displayErrorMessage("Prigioniero inserito");
						insertPrisonerView.setList(new ArrayList<String>());
					}else {
						insertPrisonerView.displayErrorMessage("Cella già piena");
					}
				}
			}
		}
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisonerView.dispose();
			new MainControllerImpl(new MainView(insertPrisonerView.getRank()));
		}
	}
	
	/**
	 * listener che aggiunge un crimine alla lista dei crimini del prigioniero
	 */
	public class AddCrimeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//recupero i crimini inseriti nella view
			List<String>list=insertPrisonerView.getList();
			//controllo che il crimine inserito non fosse gia presente
			if(list.contains(insertPrisonerView.getCombo())){
				insertPrisonerView.displayErrorMessage("Crimine già inserito");
			}
			else{
				//inserisco il crimine
				list.add(insertPrisonerView.getCombo());
				insertPrisonerView.setList(list);
			}
		}
	}
	
	public boolean isSomethingEmpty(Prisoner prisoner){
		if(prisoner.getName().isEmpty()||prisoner.getSurname().isEmpty()||prisoner.getCrimini().size()==1){
			return true;
		}
		return false;
	}

	private boolean checkDate(Prisoner prisoner){
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		if(prisoner.getInizio().after(prisoner.getFine())||prisoner.getInizio().before(today.getTime())||prisoner.getBirthDate().after(today.getTime())){
			insertPrisonerView.displayErrorMessage("Correggi le date");
			return false;
		}
		return true;
	}
}
