package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.Interfaces.ViewPrisonerController;
import main.Implementations.PrisonerManagerImpl;
import main.Interfaces.PrisonerManager;
import main.factories.ManagerFactory;
import model.Interfaces.Prisoner;
import view.Interfaces.MainView;
import view.Interfaces.ViewPrisonerView;

/**
 * controller della view prisoner view
 */
public class ViewPrisonerControllerImpl implements ViewPrisonerController{

	static ViewPrisonerView viewPrisonerView;
	private PrisonerManager prisonerManager;
	
	/**
	 * costruttore
	 * @param viewPrisonerView la view
	 */
	public ViewPrisonerControllerImpl(ViewPrisonerView viewPrisonerView){
		ViewPrisonerControllerImpl.viewPrisonerView=viewPrisonerView;
		viewPrisonerView.addViewListener(new ViewProfileListener());
		viewPrisonerView.addBackListener(new BackListener());
		prisonerManager = ManagerFactory.getPrisonerManager();
	}
	
	/**
	 * listener che si occupa di far mostrare il prigioniero
	 */
	public class ViewProfileListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			showPrisoner();
		}
		
	}

	public void showPrisoner(){
		if(String.valueOf(viewPrisonerView.getID()).isEmpty()){
			viewPrisonerView.displayErrorMessage("Devi inserire un ID");
		}else{
			Prisoner prisoner = prisonerManager.getCurrentPrisoner(viewPrisonerView.getID());
			if ( prisoner != null){
				viewPrisonerView.setProfile(prisoner.getName(), prisoner.getSurname(), prisoner.getBirthDate().toString(), prisoner.getInizio().toString(), prisoner.getFine().toString());
				viewPrisonerView.setTextArea(prisoner.getCrimini());
			}else{
				viewPrisonerView.displayErrorMessage("Prigioniero non trovato");
			}
		}
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			viewPrisonerView.dispose();
			new MainControllerImpl(new MainView(viewPrisonerView.getRank()));
		}
	}
}
